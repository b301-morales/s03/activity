package com.zuitt.activity;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {

        Scanner userInput = new Scanner(System.in);

        System.out.println();
        System.out.println("----------------------------------------------------");
        System.out.println("                WHILE LOOP METHOD");
        System.out.println("----------------------------------------------------");
        System.out.print("Input an integer whose factorial will be computed: ");
      
        try {

            int num = userInput.nextInt();
            long factorial = 1;
            int i = 1;

            if (num < 0) {
                System.out.println("Invalid input. Cannot compute for factorials of negative numbers.");
            } else {
                do {
                    factorial = factorial * i;
                    i++;
                } while (num >= i);
                System.out.println("The factorial of " + num + " is: " + factorial);
            }

        }

        catch (Exception e) {
            System.out.println("\nInvalid input. Please input a positive integer (#)");
            e.printStackTrace();
        }

        System.out.println("----------------------------------------------------");
        System.out.println();
        System.out.println();
        System.out.println("----------------------------------------------------");
        System.out.println("                  FOR LOOP METHOD");
        System.out.println("----------------------------------------------------");
        System.out.print("Input an integer whose factorial will be computed: ");
        try {

            int num = userInput.nextInt();
            long factorial = 1;
            int i = 1;
            if (num < 0) {
                System.out.println("Invalid input. Cannot compute for factorials of negative numbers.");
            } else {
                for (i = 1; i <= num; i++) {
                    factorial = factorial * i;
                }
                System.out.println("The factorial of " + num + " is: " + factorial);
            }
        }

        catch (Exception e) {
            System.out.println("Invalid input. Please input a positive integer (#)");
            e.printStackTrace();
        }
        userInput.close();
        System.out.println("----------------------------------------------------");
        System.out.println();
    }
}
